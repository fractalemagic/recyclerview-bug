package com.test;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static android.support.v7.widget.StaggeredGridLayoutManager.VERTICAL;

public class MainActivity extends Activity {

	private RecyclerView mRecyclerView;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		setContentView(R.layout.activity_main);

		mRecyclerView = (RecyclerView) findViewById(R.id.recycler);

		LayoutManager manager = new StaggeredGridLayoutManager(1, VERTICAL);
		mRecyclerView.setLayoutManager(manager);

		HolderAdapter adapter = new HolderAdapter();
		mRecyclerView.setAdapter(adapter);
	}

	private static class HolderAdapter extends RecyclerView.Adapter<ViewHolder> {

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
			Context context = parent.getContext();
			LayoutInflater inflater = LayoutInflater.from(context);
			View inflated = inflater.inflate(R.layout.recycler_item, parent, false);
			inflated.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View self) {
					MainActivity activity = (MainActivity) self.getContext();
					final RecyclerView recycler = activity.mRecyclerView;
					// Detaches the RecyclerView from its parent
					final ViewGroup parent = (ViewGroup) recycler.getParent();
					parent.removeView(recycler);
					// Re-add it after 400ms delay
					recycler.postDelayed(new Runnable() {
						@Override
						public void run() {
							parent.addView(recycler);
						}
					}, 400);
				}
			});
			return new ViewHolder(inflated);
		}

		@Override
		public void onBindViewHolder(ViewHolder holder, int position) {
			TextView item = (TextView) holder.itemView;
			item.setText("Item at " + position);
		}

		@Override
		public int getItemCount() {
			return 50;
		}

	}

	private static class ViewHolder extends RecyclerView.ViewHolder {

		public ViewHolder(View view) {
			super(view);
		}

	}

}
